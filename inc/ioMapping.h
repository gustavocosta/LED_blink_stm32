/*
 * ioMaping.h
 *
 *  Created on: 12 de abr de 2017
 *      Author: guga
 */

#ifndef IOMAPPING_H_
#define IOMAPPING_H_

typedef struct
{
  uint32_t Pin;       /*!< Specifies the GPIO pins to be configured.
                           This parameter can be any value of @ref GPIO_pins_define */

  uint32_t Mode;      /*!< Specifies the operating mode for the selected pins.
                           This parameter can be a value of @ref GPIO_mode_define */

  uint32_t Pull;      /*!< Specifies the Pull-up or Pull-Down activation for the selected pins.
                           This parameter can be a value of @ref GPIO_pull_define */

  uint32_t Speed;     /*!< Specifies the speed for the selected pins.
                           This parameter can be a value of @ref GPIO_speed_define */
}GPIO_InitTypeDef;

typedef enum
{
  GPIO_PIN_RESET = 0,
  GPIO_PIN_SET
}GPIO_PinState;

#define PERIPH_BASE          	((uint32_t)0x40000000)
#define APB2PERIPH_BASE      	(PERIPH_BASE + 0x10000)
#define GPIOC_BASE           	(APB2PERIPH_BASE + 0x1000)
#define GPIOC               	((GPIO_TypeDef *) GPIOC_BASE)

#define GPIO_PIN_13				((uint16_t)0x2000)

#define GPIO_MODE_OUTPUT_PP    	((uint32_t)0x00000001)   /*!< Output Push Pull Mode*/

#define GPIO_CRL_MODE0_1      	((uint32_t)0x00000002)   /*!< Bit 1 */
#define GPIO_SPEED_FREQ_LOW    	(GPIO_CRL_MODE0_1) 		 /*!< Low speed */

#endif /* IOMAPPING_H_ */
